function generalizedFibonacci(F0, F1, n) {
    if (n === 0) {
      return F0;
    } else if (n === 1) {
      return F1;
    }
  
    let currentFib = F1;
    let previousFib = F0;
  
    for (let i = 2; i <= Math.abs(n); i++) {
      const nextFib = currentFib + previousFib;
      previousFib = currentFib;
      currentFib = nextFib;
    }
  
    if (n < 0 && n % 2 === 0) {
      return -currentFib;
    } else {
      return currentFib;
    }
  }
  
  // Зчитуємо n від користувача через модальне вікно
  const userInput = prompt('Введіть n (порядковий номер числа Фібоначчі):');
  const n = parseInt(userInput);
  
  // Викликаємо функцію та виводимо результат на екран
  const result = generalizedFibonacci(0, 1, n);
  console.log(`n-те число Фібоначчі для n=${n} дорівнює: ${result}`);
1  